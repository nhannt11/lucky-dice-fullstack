// Khai báo thư viện express
const express = require('express');

//Import Middleware
const { prizeHistoryMiddleware } = require('../middleware/prizeHistoryMiddleware');


//Import  Controller
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById } = require('../controller/prizeHistoryController');

//Tạo router
const prizeHistoryRouter = express.Router();

//Sử dụng middleware
prizeHistoryRouter.use(prizeHistoryMiddleware);

// KHAI BÁO API

//CREATE A PrizeHistory
prizeHistoryRouter.post('/prize-histories', createPrizeHistory);


//GET ALL PrizeHistory
prizeHistoryRouter.get('/prize-histories', getAllPrizeHistory);


//GET A PrizeHistory
prizeHistoryRouter.get('/prize-histories/:prizeHistoryId', getPrizeHistoryById);


// UPDATE A PrizeHistory
prizeHistoryRouter.put('/prize-histories/:prizeHistoryId', updatePrizeHistoryById);


//DELETE A PrizeHistory
prizeHistoryRouter.delete('/prize-histories/:prizeHistoryId', deletePrizeHistoryById);





// EXPORT ROUTER
module.exports = prizeHistoryRouter;