// Khai báo thư viện mongoose
const mongoose = require('mongoose');

//  Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//  Khởi tạo Schema với các thuộc tính được yêu cầu
const prizeSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: {
        type: String,
        required: false,
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
})

module.exports = mongoose.model("prize", prizeSchema);